/** Draw grass tile into top left corner. */
window.onload = myGame;
enchant();

function myGame() {
    var game = new Core(320, 320);
    game.fps = 16;

    game.preload('map0.png');

    game.onload = function () {
        // Make a sprite big enough to cover the whole canvas.
        var bg = new Sprite(320, 320);
        
        // tilesheet will be used to access map0.png.
        var tilesheet = game.assets['map0.png'];
        
        // Create a surface as big as the background onto which we
        // can draw things.
        var surface = new Surface(bg.width, bg.height);
        
        // Set the bg image to the surface. 
        bg.image = surface;
        
        // Draw second tile in tilesheet into top left corner of surface.
        surface.draw(
            tilesheet,   // source image to copy from.
            0, 0,        // x, y coordinates in source to copy from.
            16, 16,      // w and h from source image to copy.
            0, 0,        // x, y coordinates in destination to copy to.
            16, 16       // w and h in destination to copy to.
        );
        
        game.rootScene.addChild(bg);
    };
    
    game.start();
}
